import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner teclado=new Scanner(System.in);
        int opcion,diaN,mesN,anoN;

        System.out.println("Ingrese un numero:");
        int nro=teclado.nextInt();
        Date fecha=new Date(nro);
        System.out.println("Dia de la fecha: "+fecha.getDia());
        System.out.println("Mes de la fecha: "+fecha.getMes());
        System.out.println("Ano de la fecha: "+fecha.getAno());

        do{
            System.out.println("\n\n...::::MENU PRINCIPAL::::...");
            System.out.println("1. Obtener dia de la fecha");
            System.out.println("2. Obtener mes de la fecha");
            System.out.println("3. Obtener ano de la fecha");
            System.out.println("4. Cambiar dia de la fecha");
            System.out.println("5. Cambiar mes de la fecha");
            System.out.println("6. Cambiar ano de la fecha");
            System.out.println("7. Obtener fecha en formato DD/MM/YYYY");
            System.out.println("0. Salir");

            System.out.println("Seleccione una opcion:");
            opcion=teclado.nextInt();

            switch (opcion){
                case 1-> System.out.println("Dia de la fecha: "+fecha.getDia());
                case 2-> System.out.println("Mes de la fecha: "+fecha.getMes());
                case 3-> System.out.println("Ano de la fecha: "+fecha.getAno());
                case 4 -> {
                    System.out.println("Ingrese el dia:");
                    diaN=teclado.nextInt();
                    fecha.setDia(diaN);
                }
                case 5 -> {
                    System.out.println("Ingrese el mes:");
                    mesN=teclado.nextInt();
                    fecha.setMes(mesN);
                }
                case 6 -> {
                    System.out.println("Ingrese el ano:");
                    anoN=teclado.nextInt();
                    fecha.setAno(anoN);
                }
                case 7-> System.out.println(fecha.toString());
                case 0-> System.out.println("Saliendo...");
                default -> System.out.println("Opcion invalida");
            }

        }while(opcion!=0);
    }
}
