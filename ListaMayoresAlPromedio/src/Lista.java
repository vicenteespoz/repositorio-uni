class Lista {
    private Nodo laCabeza;
    Lista() {
        laCabeza = null;
    }
    //---Inserta un objeto(int) al comienzo de la lista
    public void InsertaInicio(int o) {
        if (EstaVacia()) laCabeza=new Nodo(o, null);
        else  laCabeza = new Nodo(o, laCabeza);
    }

    //---- Inserta al final ----
    public void InsertaFinal(int o) {
        if (EstaVacia()) laCabeza=new Nodo(o, null);
        else{
            Nodo t;
            for(t = laCabeza; t.next != null; t= t.next) ;
            t.next = new Nodo(o,null);
        }
    }

    // ---cuenta la cantidad de nodos de la lista (Size)
    public int Size() {
        int tnodos=0;
        for(Nodo t = laCabeza; t !=null; t= t.next)  tnodos++;
        return tnodos;
    }

    //eliminar un nodo de la lista
// Elimina el primer nodo n tal que o.elObjeto==o
    public void Eliminar(int o) {
        if(!EstaVacia()) {
            if(laCabeza.elObjeto==o) laCabeza = laCabeza.next;
            else {
                Nodo p = laCabeza;
                Nodo t = laCabeza.next;
                while (t !=null && t.elObjeto != o)  {
                    p = t ; t = t.next;
                }
                if(t.elObjeto==o) p.next = t.next;
            }
        }
    }

    // Verifica si la lista está vacias;
    public boolean EstaVacia() {
        return laCabeza == null;
    }

    //-----Imprime la lista-----
    void Print() {
        if(laCabeza!=null) Imprimir(laCabeza);
        else System.out.println("Lista Vacia");
    }

    void Imprimir(Nodo m ) {
        if(m !=null) {m.Print(); Imprimir(m.next);}
    }

    public int promedio(){
        int promedio=0;
        Nodo p=laCabeza;
        while(p!=null){
            promedio+=p.elObjeto;
            p=p.next;
        }
        return (promedio/this.Size());
    }
    public Lista elementosMayoresAlPromedio(int promedio){
        if(laCabeza !=null){
            Lista l1=new Lista();
            Nodo p=laCabeza;
            while(p!=null){
                if(p.elObjeto>promedio){
                    l1.InsertaFinal(p.elObjeto);
                }
                p=p.next;
            }
            return l1;
        }
        return null;
    }

    public Lista ordenaDeMenorAMayor(){

        for (Nodo p=this.laCabeza;p.next!=null;p=p.next){
            if(p.elObjeto>p.next.elObjeto){
                int temp=p.next.elObjeto;
                p.next.elObjeto=p.elObjeto;
                p.elObjeto=temp;
            }
        }
        return this;
    }

    //-----Clase Nodo---------
    private class Nodo {
        public int elObjeto;
        public Nodo next;
        public Nodo(int nuevoObjeto, Nodo next)
        {this.elObjeto=nuevoObjeto; this.next = next;}
        void Print(){ System.out.print("- " + elObjeto);}
    }

}
