public class TestLista {

    public static void main(String [] args) {
        Lista l = new Lista();
        System.out.println("�Vac�a?: " + l.EstaVacia());
        System.out.println("\nSize: " + l.Size());
        l.InsertaInicio(12);
        System.out.println("�Vac�a?: " + l.EstaVacia());
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.InsertaInicio(32);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.InsertaFinal(51);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.Eliminar(12);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.Eliminar(32);
        System.out.println("\nSize: " + l.Size());
        l.Print();
        l.Eliminar(51);
        System.out.println("\nSize: " + l.Size());
        System.out.println("¿Vacia?: " + l.EstaVacia());

        //Crearé otra lista para probar el metodo
        System.out.println("--------------------------------------\n PROBANDO METODOS\n\n Lista creada:");
        Lista l1=new Lista();
        l1.InsertaInicio(1);
        l1.InsertaFinal(3);
        l1.InsertaFinal(2);
        l1.InsertaFinal(5);
        l1.InsertaFinal(10);

        l1.Print();
        System.out.println("\nLista ordenada:");
        l1.ordenaDeMenorAMayor().Print();

        System.out.println("\nPromedio: "+l1.promedio());

        System.out.println("Lista con los mayores al promedio:");
        l1.elementosMayoresAlPromedio(l1.promedio()).Print();
    }
}
