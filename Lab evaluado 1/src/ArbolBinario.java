import java.util.LinkedList;

//Bruno Caro
//Sebastian Fierro
public class ArbolBinario {

    int dato;
    ArbolBinario iz;
    ArbolBinario der;
    public ArbolBinario(int dato){
        this.dato=dato;
        iz=null;
        der=null;
    }

    public ArbolBinario(int dato, ArbolBinario iz, ArbolBinario der){
        this.dato=dato;
        this.iz=iz;
        this.der=der;
    }
    public void preOrden(){
        System.out.print(this.dato+" ");
        if(iz!=null) iz.preOrden();
        if(der!=null) der.preOrden();
    }

    public void inOrden(){
        if(iz!=null) iz.preOrden();
        System.out.print(this.dato+" ");
        if(der!=null) der.preOrden();
    }

    public void posOrden(){
        if(iz!=null) iz.preOrden();
        if(der!=null) der.preOrden();
        System.out.print(this.dato+" ");
    }

    //cantidad de nodos-1 del camino más largo de la raiz a sus hojas
    public int altura(){
        return altura(this);
    }

    private int altura(ArbolBinario raiz){
        if(raiz==null){
            return -1;
        }
        return 1 + Math.max(altura(raiz.iz),altura(raiz.der));
    }

    //size: cantidad de nodos del árbol
    public int size(){
        return size(this);
    }

    private int size(ArbolBinario raiz){
        if (raiz==null){
            return 0;
        }
        return 1 + size(raiz.iz) + size(raiz.der);
    }

    public void tree(){
        tree(this, "");
    }

    private void tree(ArbolBinario raiz, String tab) {
        if(raiz!=null){
            System.out.println(tab+"->"+raiz.dato);
            tree(raiz.iz, tab+"  |");
            tree(raiz.der, tab+"  ");
        }
    }
    //Metodo permite buscar en toda la lista los nodos del arbol cuyo valor este en el intervalo [i,j]
    public LinkedList<ArbolBinario> rango(int i, int j){
        //Lista para guardar los elementos
        LinkedList<ArbolBinario> resp = new LinkedList<>();
        if(this == null){
            return null;
        }
        //Comprueba si el nodo root es parte del rango
        if(dato >= i && dato <= j){
            resp.add(this);
        }
        //Comprueba si ambos lados del arbol es nulo, para analizar si ambos poseen datos en el rango
        if(this.iz!=null && this.der!=null){
            LinkedList<ArbolBinario> izq = this.iz.rango(i,j);
            LinkedList<ArbolBinario> dere = this.der.rango(i,j);

            for(int k = 0; k<izq.size(); k++){
                resp.add(izq.get(k));

            }
            for(int k = 0; k<dere.size(); k++){
                resp.add(dere.get(k));

            }

        }
        return resp;

    }

}


